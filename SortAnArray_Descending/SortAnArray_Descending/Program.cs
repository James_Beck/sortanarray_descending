﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAnArray_Descending
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store of variables
            string[] colours = new string[5] { "red", "blue", "orange", "black", "white" };
            string name;

            //Explanation of the program to the user
            Console.WriteLine("Hi there, what is your name?");
            name = Console.ReadLine();
            Console.WriteLine("Cool, thanks {0}. We have an array, let's sort the order");
            Console.WriteLine();
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine();
            Console.WriteLine("Now lets sort the order.");
            //Sorts the array strings alphabetically
            Array.Sort(colours);
            //Reverse the sorting order of the array. As the sort was alphabetical, this makes the order reverse alphabetical
            Array.Reverse(colours);
            Console.WriteLine("Let's see what happened.");
            Console.WriteLine();
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
        }
    }
}
